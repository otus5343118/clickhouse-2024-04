## Домашнее задание

### Работа с SQL в ClickHouse
**Цель:**

1. Отработать основные sql-операции
2. Изучить на практике особенности sql-диалекта


**Описание/Пошаговая инструкция выполнения домашнего задания:**

Последовательность шагов для выполнения ДЗ:

    1. Создать новую базу данных и перейти в нее.  
    2. Создать таблицу для бизнес-кейса "Меню ресторана" с 5+ полями, наполнить ее данными. Обязательно указывать, где нужно, модификаторы Nullable, LowCardinality и пр. Добавить комментарии.    
    3. Протестировать CRUD на созданной таблице.  
    4. Добавить несколько новых полей, удалить пару старых.  
    5. Заселектить таблицу (любую) из sample dataset 
    
<https://clickhouse.com/docs/en/getting-started/example-datasets/menus/>  
 
    6. Материализовать таблицу из п.5 (в виде таблицы)
    7. Поработать с партами. Сделать attach/detach/drop. Добавить данных в первоначально созданную таблицу.



**Запуск**

    поднять контейнер clickhouse

```bash
docker pull clickhouse/clickhouse-server

docker run -d -p 18123:8123 -p19000:9000 --name ch-server --ulimit nofile=262144:262144 clickhouse/clickhouse-server

```
    запустить клиент

```bash
docker exec -it ch-server bash
clickhouse-client
```
    Создать БД

```sql
CREATE DATABASE IF NOT EXISTS restaurante;  
USE restaurante;
```
![alt text](screens/создание%20БД.png)


    Создать таблицу "меню ресторана"

```sql
CREATE TABLE IF NOT EXISTS carta
(
    `id` UInt32,
    `name` String NULL,
    `sponsor` String,
    `event` String,
    `place` String,
    `notes` String NULL,
    `status` String,
    `page_count` UInt16,
    `dish_count` UInt16
)
ENGINE = MergeTree
PRIMARY KEY id
ORDER BY id
COMMENT 'Меню'

```
![alt text](screens/создание%20таблицы.png)
добавление данных

```sql
INSERT INTO restaurante.carta 
VALUES  (4, 'русское меню', 'ресторан русские зори', 'десерт', 'бумага', NULL, 'активно', 10, 30),
        (5, 'русское меню', 'ресторан русские зори', 'детское', 'бумага', NULL, 'активно', 10, 30),
        (6, 'русское меню', 'ресторан русские зори', 'основные блюда', 'бумага', NULL, 'активно', 10, 20),
        (7, 'английское меню', 'ресторан русские зори', 'напитки', 'бумага', NULL, 'активно', 1, 5),
        (8, 'английское меню', 'ресторан русские зори', 'напитки', 'бумага', NULL, 'активно', 1, 3);
```

![alt text](screens/добавление%20записей-2.png)

удаление записей c id = 1

```sql
ALTER TABLE carta (DELETE WHERE id=1);
```
![alt text](screens/удаление%20записей.png)
**заселектить таблицу из примера**  

необходимо создать таблицу в бд restaurante

```sql
CREATE TABLE dish
(
    id UInt32,
    name String,
    description String,
    menus_appeared UInt32,
    times_appeared Int32,
    first_appeared UInt16,
    last_appeared UInt16,
    lowest_price Decimal64(3),
    highest_price Decimal64(3)
) ENGINE = MergeTree ORDER BY id;
```
скачать файлы с данными (находясь в контейнере clickhouse)

```bash
wget https://s3.amazonaws.com/menusdata.nypl.org/gzips/2021_08_01_07_01_17_data.tgz
```
распаковать файл
```bash
tar xvf 2021_08_01_07_01_17_data.tgz
```
загрузить таблицу данными из csv

```bash
clickhouse-client --format_csv_allow_single_quotes 0 --input_format_null_as_default 0 --query "INSERT INTO dish FORMAT CSVWithNames" < Dish.csv
```
![alt text](screens/download%20dataset.png)

проверяем загрузку

```sql
select * from restaurante.dish limit 10
```
![alt text](screens/select%20данных%20из%20примера.png)

создание view (первые 10 строк таблицы)

```sql
CREATE VIEW dish_limit AS
    SELECT * 
    FROM restaurante.dish 
    LIMIT 10;

```
результат view

![alt text](screens/view.png)

**создание mat view**
перед созданием mat view создадим таблицу, аналогичной dish

```sql
CREATE TABLE dish_4mv
(
    id             UInt32,
    name           String,
    description    String,
    menus_appeared UInt32,
    times_appeared Int32,
    first_appeared UInt16,
    last_appeared  UInt16,
    lowest_price   Decimal(18, 3),
    highest_price  Decimal(18, 3)
)
    ENGINE = MergeTree ORDER BY id
```
создадим mat view, запрос которого будет отбирать блюда после 2000 года и начиная по стоймости 100 usd

```sql
CREATE MATERIALIZED VIEW mat_view_dish TO dish_4mv AS
SELECT *
FROM dish
WHERE lowest_price > 100 AND dish.first_appeared > 2000;

```

добавим записи в таблицу блюд

```sql
INSERT INTO dish (id, name, description, menus_appeared, times_appeared, first_appeared, last_appeared, lowest_price, highest_price)
VALUES  (1587952, 'Акула в сметане', '', 1, 0, 2004, 2004, 134.000, 134.000),
        (1587973, 'Енотовые роллы', '', 1, 0, 1999, 2004, 120.000, 120.000),
        (1587974, 'Маргбурские раковины ', '', 1, 1, 2012, 2012, 260.000, 260.000);

```
После добавления записей мы видим, что блюдо "Енотовые роллы" не попала во view, т.к. не попадает под условие запроса.

![alt text](screens/materialized_view.png)