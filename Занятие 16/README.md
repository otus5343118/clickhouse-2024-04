
# Домашнее задание

## Мутации данных и манипуляции с партициями

**Цель:**

    1. Понять, как работают мутации данных в ClickHouse.
    2. Научиться управлять партициями таблиц и выполнять операции с ними.
---

## Описание/Пошаговая инструкция выполнения домашнего задания:

### 1. Создание таблицы:

Создайте таблицу user_activity с полями:

* user_id (UInt32) — идентификатор пользователя
* activity_type (String) — тип активности (например, 'login', 'logout', 'purchase')
* activity_date (DateTime) — дата и время активности

Используйте MergeTree как движок таблицы и настройте партиционирование по дате активности (activity_date).

### 2. Заполнение таблицы:
Вставьте несколько записей в таблицу user_activity. Используйте различные user_id, activity_type и activity_date.

### 3. Выполнение мутаций:
Выполните мутацию для изменения типа активности у пользователя(-ей)

### 4. Проверка результатов:
Напишите запрос для проверки изменений в таблице user_activity.   
Убедитесь, что тип активности у пользователей изменился. Приложите логи отслеживания мутаций в системной таблице.

### 5. Манипуляция с партициями:
Удалите партицию за определённый месяц.

### 6. Проверка состояния таблицы:
Проверьте текущее состояние таблицы после удаления партиции. Убедитесь, что данные за указанный месяц были удалены.


**Дополнительные задания (по желанию):**

* Исследуйте, как работают другие типы мутаций.  
* Попробуйте создать новую партицию и вставить в неё данные.

* Изучите возможность использования TTL (Time to Live) для автоматического удаления старых партиций.

## Выполнение ДЗ

### 1-2. Запуск Clickhouse и создание таблицы

Для запуска СH воспользуемся [ClickHouse docker compose recipes](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/README.md) и выберем [ClickHouse single node with Keeper](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/recipes/ch-1S_1K/README.md) 

После развертывания clickhouse будет автоматически создана таблица user_activity. 

```sql
CREATE TABLE IF NOT EXISTS user_activity (
    `user_id`  UInt32,
    `activity_type`  String,
    `activity_date`  DateTime,
)
ENGINE = MergeTree
PARTITION BY toYYYYMM(`activity_date`)
ORDER BY (`user_id`);


```

Для этого в docker-compose примонтирован соответствующий том, с вышеуказанным запросом

```yaml
  - ${PWD}/fs/volumes/clickhouse/ddl/create_table.sql:/docker-entrypoint-initdb.d/create_table.sql
```

для генерации тестовых данных используется следующий запрос:

```sql
INSERT INTO user_activity(user_id, activity_type, activity_date)
VALUES
    (1,'login','2024-07-08 11:16:47'),
    (1,'logout','2024-07-08 11:20:23'),
    (1,'login','2024-07-08 13:01:47'),
    (1,'purchase','2024-07-08 13:16:47'),
    (1,'logout','2024-07-08 13:30:47'),
    (2,'login','2024-07-08 11:20:47'),
    (2,'purchase','2024-07-08 11:25:23'),
    (2,'logout','2024-07-08 11:29:47'),
    (3,'login','2024-08-18 12:19:47'),
    (3,'logout','2024-08-18 12:23:23'),
    (3,'login','2024-08-18 14:04:47'),
    (3,'purchase','2024-08-18 14:20:47'),
    (3,'logout','2024-08-18 14:33:47'),
    (4,'login','2024-08-18 14:23:47'),
    (4,'purchase','2024-08-18 14:28:23'),
    (4,'logout','2024-08-18 14:32:47');
``` 

![alt text](screens/image1.png)


### 3-4. Выполнение мутаций

Для создания мутаций изменим значение activity_type для одной из записей

```sql
ALTER TABLE user_activity
UPDATE activity_type='signup'
WHERE 
    user_id = 4 AND activity_type='login';
```

Проверим, что данные изменились
![alt text](screens/image2.png)

Смотрим системную таблицу
![alt text](screens/image3.png)

### 5. Манипуляции с партициями

Удалим партицию за август месяц

```sql
ALTER TABLE user_activity DROP PARTITION '202408';
``` 

![alt text](screens/image4.png)

На скрине видим, что партиция за август месяц стала не активной, но через некоторое время будет удалена

![alt text](screens/image5.png)

Смотрим данные

![alt text](screens/image6.png)

На скрине показано, что данные за август месяц удалились, остались только за июль.




---

<u>**Дополнительные источники**</u>

1. <https://datafinder.ru/products/zametki-pro-clickhouse-tutorial-101-bolshaya-podborka-informacii#9>
