## Домашнее задание

### Работа с движками семейства MergeTree
**Цель:**

Изучить принципы работы одного из главных семейств движков ClickHouse - MergeTree.
Тренируемые навыки:

- понимание, для каких задач какой движок пригодится лучше всего;  
- работа по дедуплицировании данных и замене привычных delete/update.


**Описание/Пошаговая инструкция выполнения домашнего задания:**

<https://docs.google.com/document/d/1lpwH2-HjRFx3VRYFSx8Jc9ww9UGBgw_91d16fjIxgDQ/edit?usp=sharing>

**Условия ДЗ**

1. По заданным описаниям таблиц и вставки данных определить используемый движок 
2. Заполнить пропуски, запустить код
3. Сравнить полученный вывод и результат из условия

---
---
<u>**Пример #1 - VersionCollapsingMergeTree**</u>

```sql
CREATE TABLE tbl1
(
    UserID UInt64,
    PageViews UInt8,
    Duration UInt8,
    Sign Int8,
    Version UInt8
)
ENGINE = <ENGINE>
ORDER BY UserID;

INSERT INTO tbl1 VALUES (4324182021466249494, 5, 146, -1, 1);
INSERT INTO tbl1 VALUES (4324182021466249494, 5, 146, 1, 1),(4324182021466249494, 6, 185, 1, 2);
```

```sql
SELECT * FROM tbl1;
```
![alt text](screens/image1.png)

В данном примере используется движок ***VersionedCollapsingMergeTree***, поскольку сама структура таблицы и запросов на вставку предполагает удаление строк с использованием версий. Используется атрибут sign, который имеет значение 1 и -1 для строк, которые вставляются далее. Продемонстрируем это на примере:

```sql
CREATE TABLE tbl1
(
    UserID UInt64,
    PageViews UInt8,
    Duration UInt8,
    Sign Int8,
    Version UInt8
)
ENGINE = VersionedCollapsingMergeTree(Sign,Version)
ORDER BY UserID;
```
![alt text](screens/example1-VersionedCollapsingMergeTree.png)

Данные в таблице:

![alt text](screens/example1-VersionedCollapsingMergeTree-select.png)


<u>**Пример #2 - SummingMergeTree**</u>

```sql
CREATE TABLE tbl2
(
    key UInt32,
    value UInt32
)
ENGINE = <ENGINE>
ORDER BY key;

INSERT INTO tbl2 Values(1,1),(1,2),(2,1);
```

```sql
SELECT * FROM tbl2;
```
![alt text](screens/image2.png)

В данном примере используется движок ***SummingMergeTree***, поскольку значения в результирующей таблице сгруппированны по полю key, а также можно увидеть сумму значений атрибута value.   
Продемонстрируем это на примере:

```sql
CREATE TABLE tbl2
(
    key UInt32,
    value UInt32
)
ENGINE = SummingMergeTree(value)
ORDER BY key;

```

![alt text](screens/example2-SummingMergeTree.png)


<u>**Пример #3 - ReplacingMergeTree**</u>

```sql
CREATE TABLE tbl3
(
    `id` Int32,
    `status` String,
    `price` String,
    `comment` String
)
ENGINE = <ENGINE>
PRIMARY KEY (id)
ORDER BY (id, status);

INSERT INTO tbl3 VALUES (23, 'success', '1000', 'Confirmed');
INSERT INTO tbl3 VALUES (23, 'success', '2000', 'Cancelled'); 
```

```sql
SELECT * from tbl3 WHERE id=23;
```
![alt text](screens/image3-1.png)

```sql
SELECT * from tbl3 FINAL WHERE id=23;
```
![alt text](screens/image3-2.png)



В данном примере используется движок ***ReplacingMergeTree***, 
который удаляет дубликаты записей с одинаковым значением **ключа сортировки**.  
В этом примере мы вставляем две записи с одинаковым ключом сортировки ('23','success'). После слияния, останется последняя запись. Собственно, продемонстриуем это на примере:

```sql
CREATE TABLE tbl3
(
    `id` Int32,
    `status` String,
    `price` String,
    `comment` String
)
ENGINE = ReplaceMergeTree
PRIMARY KEY (id)
ORDER BY (id, status);

```

![alt text](screens/example3-ReplacingMergeTree.png)

![alt text](screens/example3-ReplacingMergeTree-select.png)

<u>**Пример #4 - MergeTree**</u>

```sql
CREATE TABLE tbl4
(   CounterID UInt8,
    StartDate Date,
    UserID UInt64
) ENGINE = <ENGINE>
PARTITION BY toYYYYMM(StartDate) 
ORDER BY (CounterID, StartDate);

INSERT INTO tbl4 VALUES(0, '2019-11-11', 1);
INSERT INTO tbl4 VALUES(1, '2019-11-12', 1);
```
В данном случае используется движок ***MergeTree***. Здесь просто задается партиционирование по дате для дальнейшего использования в следующем запросе.

![alt text](screens/example4-MergeTree.png)

<u>**Пример #5 - AggregatingMergeTree**</u>
```sql
CREATE TABLE tbl5
(   CounterID UInt8,
    StartDate Date,
    UserID AggregateFunction(uniq, UInt64)
) ENGINE = <ENGINE>
PARTITION BY toYYYYMM(StartDate) 
ORDER BY (CounterID, StartDate);

INSERT INTO tbl5
select CounterID, StartDate, uniqState(UserID)
from tbl4
group by CounterID, StartDate;

INSERT INTO tbl5 VALUES (1,'2019-11-12',1);

SELECT uniqMerge(UserID) AS state 
FROM tbl5 
GROUP BY CounterID, StartDate;

```
![alt text](screens/image4.png)

В данном примере используется движок ***AggregatingMergeTree***, поскольку в таблице tbl5 мы видим описание типа AggregateFunction  

```sql
CREATE TABLE tbl5
(   CounterID UInt8,
    StartDate Date,
    UserID AggregateFunction(uniq, UInt64)
) ENGINE = AggregatingMergeTree
PARTITION BY toYYYYMM(StartDate) 
ORDER BY (CounterID, StartDate);
```
![alt text](screens/example5-AggregatingMergeTree.png)

Ошибка 53 возникает из-за несоответствия типов. В примере была вставка значения -1 в поле UserID, однако, это значение должно быть типа AggregateFuncion 

<u>**Пример #6 - CollapsingMergeTree**</u>

```sql
CREATE TABLE tbl6
(
    `id` Int32,
    `status` String,
    `price` String,
    `comment` String,
    `sign` Int8
)
ENGINE = <ENGINE>
PRIMARY KEY (id)
ORDER BY (id, status);

INSERT INTO tbl6 VALUES (23, 'success', '1000', 'Confirmed', 1);
INSERT INTO tbl6 VALUES (23, 'success', '1000', 'Confirmed', -1), (23, 'success', '2000', 'Cancelled', 1);

SELECT * FROM tbl6;
```
![alt text](screens/image6-1.png)

```sql
SELECT * FROM tbl6 FINAL;
```
![alt text](screens/image6-2.png)

В данном случае используется движок ***CollapsingMergeTree***, поскольку из результирующих данных примера (после FINAL) видно, что строки, у которых есть пара sign (-1,1), а также совпадение по (id,status) удаляются - остается одна строка. Продемонстрируем это на примере:

```sql
CREATE TABLE tbl6
(
    `id` Int32,
    `status` String,
    `price` String,
    `comment` String,
    `sign` Int8
)
ENGINE = CollapsingMergeTree(sign)
PRIMARY KEY (id)
ORDER BY (id, status);
```
![alt text](screens/example6-CollapsingMergeTree.png)

![alt text](screens/example6-CollapsingMergeTree-select.png)

В целом, трудностей не возникло. 
Что помогло - достаточно внимательно посмотреть на запросы и на скрины итогового результата. Опираясь на это можно догадаться о типе движка.
