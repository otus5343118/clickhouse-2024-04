
# Домашнее задание

## Репликация и другие фоновые процессы
**Формулировка ДЗ:**


1) Взять любой демонстрационный DATASET, не обязательно полный набор данных: <https://clickhouse.com/docs/en/getting-started/example-datasets>  

2) Конвертировать таблицу в реплицируемую, используя макрос replica 

3) Добавить 2 реплики

4) отдать результаты запросов как 2 файла

```sql
SELECT
getMacro(‘replica’),
*
FROM remote(’разделенный запятыми список реплик’,system.parts)
FORMAT JSONEachRow;

SELECT * 
FROM system.replicas FORMAT JSONEachRow;
```
5) Добавить/выбрать колонку с типом Date в таблице, добавить TTL на таблицу «хранить последние 7 дней». Предоставить результат запроса «SHOW CREATE TABLE таблица» на проверку.

---

## Описание/Пошаговая инструкция выполнения домашнего задания:


### Запуск CH

Для запуска СH воспользуемся [ClickHouse docker compose recipes](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/README.md) и выберем [ClickHouse cluster cluster_1S_2R](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/recipes/cluster_1S_2R/README.md) 

запустить клиент

```bash
docker exec -it clichouse-01 bash
clickhouse-client

docker exec -it clichouse-02 bash
clickhouse-client

```

посмотрим топологию кластера

```sql
SELECT
    host_name,
    host_address,
    replica_num,
    cluster
FROM system.clusters;
```
![alt text](screens/image1.png)

## хост clickhouse-01

### 1) Загрузка тестовых данных 

Для примера, возьмем демонстрационный dataset "What's on the Menu?"
<https://clickhouse.com/docs/en/getting-started/example-datasets/menus>  

После создания таблиц и импорта получаем на хосте clickhouse-01 следующие таблицы:
```sql
SELECT
    database,
    `table`,
    formatReadableSize(sum(data_compressed_bytes) AS size) AS compressed,
    formatReadableSize(sum(data_uncompressed_bytes) AS usize) AS uncompressed,
    round(usize / size, 2) AS compr_rate,
    sum(rows) AS rows,
    count() AS part_count
FROM system.parts
WHERE (active = 1) AND (database = 'default') AND (`table` LIKE '%')
GROUP BY
    database,
    `table`
ORDER BY size DESC;
```

![alt text](screens/image2.png)

### 2) Конвертация таблицы в реплицируемые

Для конвертации таблиц MergeTree в реплицируемые, нам потребуется узнать partition_id таблиц. Для этого выполним запрос:

```sql
SELECT DISTINCT
    partition_id,
    database,
    `table`
FROM system.parts
WHERE database = 'default'
```
![alt text](screens/image3-partition_id.png)

Создадим таблицу dish_replic на основе таблицы dish. Для этого выполним запрос:

```sql
CREATE TABLE dish_replic AS dish 
ENGINE=ReplicatedMergeTree('/clickhouse/table/shard_{shard}/{database}/{table}','{replica}')
ORDER BY id;
```
![alt text](screens/image4.png)

далее, скопируем партиции из dish в dish_replic
```sql
ALTER TABLE dish_replic ATTACH PARTITION ID 'all' FROM dish;
```
где 'all' - имя партиции для таблицы dish.

удалим старую таблицу и вернем старое имя (dish) реплицированной таблице:
```sql
DROP TABLE dish;
RENAME TABLE dish_replic TO dish;
```
На скрине ниже информация о результатах
![alt text](screens/image5.png)

Видно, что таблица dish стала реплицированной.
```sql
SELECT *
FROM system.replicas FORMAT VERTICAL;
```
![alt text](screens/image6.png)

### Повторим тоже самое на ноде clickhouse-02

смотрим, что на ноде 2, таблица dish тоже реплицируемая
![alt text](screens/image7.png)

### Проверим, как работает репликация

Для этого, добавим данные в таблицу dish на ноде 1
```sql
INSERT INTO dish (id, name, description, menus_appeared, times_appeared, first_appeared, last_appeared, lowest_price, highest_price)
VALUES  (2024001, 'Акула в сметане', '', 1, 0, 2004, 2004, 134.000, 134.000),
        (2024002, 'Енотовые роллы', '', 1, 0, 1999, 2004, 120.000, 120.000),
        (2024003, 'Маргбурские раковины ', '', 1, 1, 2012, 2012, 260.000, 260.000);
```
видим, что данные записались в ноду 1
![alt text](screens/image8-node1.png)

также видно, что данные автоматически реплицировались в ноду 2
![alt text](screens/image9-node2.png)

### 4) отдать результаты запроса в файл

```sql
SELECT
    getMacro('replica'),
    *
FROM remote('clickhouse-01,clickhouse-02', system.parts)
INTO OUTFILE 'result1.json'
FORMAT JSONEachRow;
```
Результат запроса можно посмотреть в файле [result1.json](./out/result1.json) 

```sql
SELECT * 
FROM system.replicas FORMAT JSONEachRow;
INTO OUTFILE 'node1-result2.json
FORMAT JSONEachRow;
```

Результат для ноды 1 [node1-result2.json](./out/node1-result2.json)         
Результат для ноды 2 [node2-result2.json](./out/node2-result2.json)

### 5) добавим TTL на таблицу
поскольку в таблице нет атрибута datetime, то добавим его
```sql
ALTER TABLE dish ADD COLUMN ttl_dish DateTime;
ALTER TABLE dish MODIFY TTL ttl_dish + INTERVAL 7 DAY;
```   
результат
![alt text](screens/image10.png)

---

<u>**Дополнительные источники**</u>

1. <https://mrkaran.dev/posts/clickhouse-replication/>

2. <https://github.com/mr-karan/clickhouse-keeper-example?tab=readme-ov-file>
