
# Домашнее задание

## Шардирование и распределенные запросы
**Формулировка ДЗ:**



1) запустить N экземпляров clickhouse-server
    
2) описать несколько (2 или более) топологий объединения экземпляров в шарды в конфигурации clickhouse на одном из экземпляров. Фактор репликации и количество шардов можно выбрать на свой вкус.

3) предоставить xml-секцию для проверки текстовым файлом
    
4) создать DISTRIBUTED-таблицу на каждую из топологий. Можно использовать системную таблицу system.one, содержащую одну колонку dummy типа UInt8, в качестве локальной таблицы.  
    или   

5) предоставить вывод запроса SELECT *,hostName(),_shard_num from distributed-table для каждой distributed-таблицы, можно добавить group by и limit по вкусу если тестовых данных много.  

    или     

    предоставить SELECT * FROM system.clusters; 
    SHOW CREATE TABLE для каждой Distributed-таблицы.
    п.5 можно любой из на ваш выбор из «или», можно оба



---

## Описание/Пошаговая инструкция выполнения домашнего задания:


### Запуск CH

Для запуска СH воспользуемся [ClickHouse docker compose recipes](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/README.md) и выберем [ClickHouse  cluster_2S_1R](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/recipes/cluster_2S_1R/README.md) 

запустить клиент

```bash
docker exec -it clichouse-01 clickhouse-client
docker exec -it clichouse-02 clickhouse-client

```

посмотрим топологию кластера

```sql
SELECT
    host_name,
    host_address,
    shard_num,
    shard_weight,
    replica_num,
    cluster
FROM system.clusters;
```
![alt text](screens/image1.png)

Топология кластера прописана в config.xml каждого инстанса CH
Полный список файлов конфигурации можно посмотреть в репозитории к данному заданию.  

```xml
    <remote_servers>
        <cluster_2S_1R>
            <shard>
                <replica>
                    <host>clickhouse-01</host>
                    <port>9000</port>
                </replica>
            </shard>
            <shard>
                <replica>
                    <host>clickhouse-02</host>
                    <port>9000</port>
                </replica>
            </shard>
        </cluster_2S_1R>
    </remote_servers>
```

## хост clickhouse-01

### Создание DISTRIBUTED-таблицы 

Distributed таблицу будем делать на основе таблицы system.one

```sql
CREATE TABLE system.one_distributed
(
    dummy UInt8;
)
ENGINE=Distributed('cluster_2S_1R','system','one',dummy);
```
результат

![alt text](screens/image2.png)

![alt text](screens/image3.png)


---

<u>**Дополнительные источники**</u>

1. <https://habr.com/ru/companies/digitalleague/articles/759316/>

2. <https://clickhouse.com/docs/en/engines/table-engines/special/distributed>
