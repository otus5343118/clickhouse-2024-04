
# Домашнее задание

## Профилирование запросов
**Формулировка ДЗ:**


1) Выполнить запрос с WHERE не использующим ПК. Выполнить запрос с WHERE
использующим ПК. Сравнить text_log запросов, предоставить строки лога относящиеся к
пробегу основного индекса.
2) Показать тот же индекс через EXPLAIN

---

## Описание/Пошаговая инструкция выполнения домашнего задания:


Для запуска СH воспользуемся [ClickHouse docker compose recipes](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/README.md) и выберем [ClickHouse single node with Keeper](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/recipes/ch-1S_1K/README.md) 

Для демонстрации будем использовать датасет [Brown University Benchmark](https://clickhouse.com/docs/ru/getting-started/example-datasets/brown-benchmark)

### Выполнение запросов

Будем работать с таблицей logs2;

```sql
CREATE TABLE mgbench.logs2 (
  log_time    DateTime,
  client_ip   IPv4,
  request     String,
  status_code UInt16,
  object_size UInt64
)
ENGINE = MergeTree()
ORDER BY log_time;
```

для включения trace log, в clickhouse-client необходимо выполнить команду:

```sql
set send_logs_level='trace';
```


выполним запрос с WHERE, где используется PrimaryKey
при выполнении запроса будем показывать только trace log

![alt text](screens/image1.png)

В trace loge мы видим строки, которые указывают, что использовался индекс

![alt text](screens/image2.png)

Выполним запросс WHERE, где не используется PrimaryKey

![alt text](screens/image3.png)

В trace loge мы видим строки: Key condition: unknown

![alt text](screens/image4.png)

далее, выполним explain запросов

![alt text](screens/image5.png)

из рисунка видно, что explain показывает использование индекса, что совпадает с рис.1

далее, выполним explain запроса без использования primary key

![alt text](screens/image6.png)




---

<u>**Дополнительные источники**</u>

1. <https://clickhouse.com/docs/en/operations/system-tables/trace_log>
