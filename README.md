# ClickHouse-2024-04 (курс OTUS)

## ClickHouse для инженеров и архитекторов БД

Данный репозиторий содержит ДЗ курса и исходный код финального проекта


### Домашние задания

- [ ] [Занятие 2](https://gitlab.com/otus5343118/clickhouse-2024-04/-/blob/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%202/task2.md?ref_type=heads) Область применения и первое представление 
- [ ] [Занятие 3](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%203?ref_type=heads) 
Развертывание и базовая конфигурация, интерфейсы и инструменты 
- [ ] [Занятие 5](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%205?ref_type=heads) 
Язык SQL
- [ ] [Занятие 6](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%206/HW1?ref_type=heads) 
Функции для работы с типами данных, агрегатные функции и UDF
- [ ] [Занятие 7](https://gitlab.com/otus5343118/clickhouse-2024-04/-/blob/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%207/README.md?ref_type=heads) 
Движки MergeTree Family  
- [ ] [Занятие 9](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%209?ref_type=heads) 
Словари, оконные и табличные функции
- [ ] [Занятие 10](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%2010?ref_type=heads) 
JOINS и агрегации
- [ ] [Занятие 12](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%2012?ref_type=heads) 
Проекции и материализованные представления  
- [ ] [Занятие 13](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%2013?ref_type=heads) 
Репликация и другие фоновые процессы  
- [ ] [Занятие 14](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%2014?ref_type=heads) 
Шардирование и распределенные запросы
- [ ] [Занятие 16](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%2016?ref_type=heads) 
Мутации данных и манипуляции с партициями     
- [ ] [Занятие 17](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%2017?ref_type=heads) 
RBAC контроль доступа
- [ ] [Занятие 20](https://gitlab.com/otus5343118/clickhouse-2024-04/-/tree/main/%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%2020?ref_type=heads) 
Профилирование запросов
