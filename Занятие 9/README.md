## Домашнее задание

### Словари, оконные и табличные функции
**Формулировка ДЗ:**

1) Создать таблицу с полями:  
user_id UInt64,  
action String,  
expense UInt64  

2) Создать словарь, в качестве ключа user_id, в качестве атрибута email String, источник словаря любой вам удобный, например file.  

3) Наполнить таблицу и источник любыми данными, с низкоардинальными значениями для поля action и хотя бы по несколько повторящихся строк для каждого user_id

4) написать SELECT, возвращающий:
- email при помощи dictGet,
- аккамулятивную сумму expense, c окном по action
- сортировка по email 
---

**Описание/Пошаговая инструкция выполнения домашнего задания:**


<u>**Запуск CH**</u>

поднять контейнер clickhouse

```bash
docker pull clickhouse/clickhouse-server

docker run -d -p 18123:8123 -p19000:9000 --name ch-server --ulimit nofile=262144:262144 clickhouse/clickhouse-server

```
запустить клиент

```bash
docker exec -it ch-server bash
clickhouse-client
```

<u>**Создание таблицы**</u>

```sql
CREATE TABLE user
(
    user_id UInt64,
    action String,
    expense UInt64,
)
ENGINE = MergeTree
ORDER BY user_id;

```
![alt text](screens/image1.png)

<u>**Создание словаря**</u>

Создадим файл словаря (dict.csv) вида:

```csv
1,"user1@mail.ru"
2,"user2@yandex.ru"
5,"user5@gmail.com"
8,"test@yahoo.com"
```
Запишем его в контейнер, иначе clickhouse его не найдет

```bash
docker cp dict.csv 2863dcee5b34:/var/lib/clickhouse/user_files/dict.csv 
```

Создадим словарь:

```sql
CREATE DICTIONARY email_dict (
    user_id UInt64,
    email String
)
PRIMARY KEY user_id
SOURCE(FILE(path '/var/lib/clickhouse/user_files/dict.csv' format 'CSV'))
LAYOUT(FLAT)
LIFETIME(3600)
```

проверим, создался ли словарь

```sql
SELECT * FROM email_dict; 
SELECT * FROM system.dictionaries\G;
```
![alt text](screens/image2.png)

<u>**Наполним таблицу данными**</u>

```sql
INSERT INTO user 
VALUES  (1, 'visit', 10),
        (1, 'visit', 30),
        (8, 'visit', 5),
        (8, 'edit', 20),
        (1, 'visit', 1),
        (2, 'visit', 30),
        (1, 'edit', 100),
        (2, 'visit', 30),
        (2, 'buy', 40),
        (1, 'visit', 20),
        (1, 'visit', 30),
        (5, 'buy', 10),
        (5, 'visit', 5),
        (8, 'visit', 5),

```
![alt text](screens/image3.png)

<u>**SELECT запросы:**</u>

получим email при помощи dictGet

```sql
SELECT 
    user_id,
    action,
    expense,
    dictGet('email_dict','email',user_id)
FROM user

```

![alt text](screens/image4.png)

получим аккомулятивную сумму expense, c окном по action с сортировкой по email

```sql
SELECT
    user_id,
    action,
    expense,
    dictGet('email_dict','email',user_id) AS email,
    SUM(expense) OVER (PARTITION BY action ORDER BY email) AS sum_expense
FROM user
```
![alt text](screens/image5.png)

---

<u>**Дополнительные источники**</u>

1. <https://www.youtube.com/watch?v=BMzefJekPC0>

2. <https://bilab.ru/rabota-s-vneshnimi-slovariami-v-clickhouse>