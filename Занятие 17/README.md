
# Домашнее задание

## RBAC Контроль доступа, квоты, ограничения
**Формулировка ДЗ:**

1) Создать пользователя john с паролем «qwery»

2) Создать роль devs

3) Выдать роли devs права на SELECT на любую таблицу

4) выдать роль devs пользователю john

5) предоставить результаты SELECT из system-таблиц соответсвующих созданным
сущностям

---

## Описание/Пошаговая инструкция выполнения домашнего задания:


### Запуск CH

Для запуска СH воспользуемся [ClickHouse docker compose recipes](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/README.md) и выберем [ClickHouse  cluster_2S_1R](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/recipes/cluster_2S_1R/README.md) 

запустить клиент

```bash
docker exec -it clichouse-01 clickhouse-client
docker exec -it clichouse-02 clickhouse-client

```

посмотрим топологию кластера

```sql
SELECT
    host_name,
    host_address,
    shard_num,
    shard_weight,
    replica_num,
    cluster
FROM system.clusters;
```
![alt text](screens/image1.png)

## хост clickhouse-01

### 1) Создание пользователя c паролем 'querty'

```sql
CREATE USER IF NOT EXISTS john ON CLUSTER 'cluster_2S_1R' IDENTIFIED BY 'qwerty'
```
результат

![alt text](screens/image2.png)

### 2) Создание роли devs

```sql
CREATE ROLE IF NOT EXISTS dev ON CLUSTER 'cluster_2S_1R'
```
выдать права на таблицу 
```sql
GRANT ON CLUSTER 'cluster_2S_1R' SELECT ON system.one TO dev 
```
выдать роль пользователю john
```sql
GRANT ON CLUSTER 'cluster_2S_1R' dev TO john
```
результат

![alt text](screens/image3.png)


![alt text](screens/image4.png)


---

<u>**Дополнительные источники**</u>

1. <https://clickhouse.com/docs/ru/sql-reference/statements/create/user>

2. <https://clickhouse.com/docs/ru/sql-reference/statements/create/role>
