
# Домашнее задание

## Проекции и материализованные представления

Переливка данных через ETL-инструменты  

**Цель:**

    1. Понять, как работают проекции и материализованные представления в ClickHouse.
    2. Научиться создавать и использовать их для оптимизации запросов.
---

## Описание/Пошаговая инструкция выполнения домашнего задания:

### 1. Создание таблицы:

Создайте таблицу sales с полями:

* id (UInt32) — уникальный идентификатор продажи
* product_id (UInt32) — идентификатор продукта
* quantity (UInt32) — количество проданных единиц
* price (Float32) — цена за единицу
* sale_date (DateTime) — дата продажи

Заполните таблицу тестовыми данными.

### 2. Создание проекции:

Создайте проекцию для таблицы sales, которая будет агрегировать данные по product_id и считать общую сумму продаж (количество и сумма по цене) за каждый продукт.

### 3. Создание материализованного представления:

Создайте материализованное представление sales_mv, которое будет автоматически обновляться при вставке новых данных в таблицу sales. Оно должно хранить общие продажи по продуктам с полями:
    
* product_id
* total_quantity
* total_sales

### 4. Запросы к данным:

Напишите запрос, который извлекает данные из проекции sales_projection.

Напишите запрос, который извлекает данные из материализованного представления sales_mv.

### 5. Сравнение производительности:

Сравните время выполнения запроса к основной таблице sales с запросом к проекции sales_projection и материализованному представлению sales_mv. Обратите внимание на разницу в производительности.


**Дополнительные задания (по желанию):**

Исследуйте, как изменения в основной таблице влияют на проекции и материализованные представления.

Попробуйте использовать разные агрегатные функции в проекциях и представлениях.

## Выполнение ДЗ

### 1. Запуск Clickhouse и создание таблицы

Для запуска СH воспользуемся [ClickHouse docker compose recipes](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/README.md) и выберем [ClickHouse single node with Keeper](https://github.com/ClickHouse/examples/blob/main/docker-compose-recipes/recipes/ch-1S_1K/README.md) 

После развертывания clickhouse будет автоматически создана таблица sales. 

```sql
CREATE TABLE IF NOT EXISTS sales (
    `id`  UInt32,
    `product_id`  UInt32,
    `quantity`  UInt32,
    `price` Float32,
    `sale_date` DateTime,
) ENGINE = MergeTree() ORDER BY (`id`);

```

Для этого в docker-compose примонтирован соответствующий том, с вышеуказанным запросом

```yaml
  - ${PWD}/fs/volumes/clickhouse/ddl/create_table.sql:/docker-entrypoint-initdb.d/create_table.sql
```

для генерации тестовых данных (200 записей) используется следующий запрос:

```sql
INSERT INTO sales
SELECT
    number as id,
    floor(randUniform(1, 200)) AS product_id,
    floor(randUniform(1, 100)) AS quantity,
    round(rand()%100 * 1.99, 2) AS price, 
    toDateTime('2024-08-23 00:00:00') + rand() % (number+1)-(((12 + randPoisson(12)) * 60) * 60)-(((12 + randPoisson(12))) * 60) as sale_date 
FROM numbers(1,200);
``` 

![alt text](screens/image1.png)


### 2. Создание проекции

Для создания проекции используется следующий запрос

```sql
ALTER TABLE sales ADD PROJECTION projection_sales (
SELECT
    product_id,
    sum(quantity) as total_quantity,
    round(sum(quantity*price),2) as total_sales
GROUP BY product_id
);

ALTER TABLE sales MATERIALIZE PROJECTION projection_sales;
     
```

Проверим, что проекция создалась
![alt text](screens/image2.png)

Проверим, что при выполнее запроса, используется проекция. Для этого выполним explain
![alt text](screens/image3.png)

### 3. Создание материализованного представления

Для создания MV необходимо создать целевую таблицу для хранения общих продаж по продуктам

```sql
CREATE TABLE IF NOT EXISTS sales_summary (
    product_id UInt32,
    total_quantity UInt32,
    total_sales Float32
) ENGINE = MergeTree()
ORDER BY (product_id);
``` 
![alt text](screens/image4.png)

после этого, необходимо выполнить запрос на создание MV

```sql
CREATE MATERIALIZED VIEW IF NOT EXISTS sales_mv
TO sales_summary AS
SELECT
    product_id,
    sum(quantity) AS total_quantity,
    round(sum(quantity * price),2) AS total_sales
FROM sales
GROUP BY product_id;
```
![alt text](screens/image5.png)

После создания MV, мы можем убедиться, что оно не содержит данных, поскольку данные там появятся только после вставки в исходную таблицу

![alt text](screens/image6.png)

Для того, чтобы данные появились необходимо записать данные в таблицу. Выполним запрос на вставку

```sql
INSERT INTO sales
SELECT
    number as id,
    floor(randUniform(1, 200)) AS product_id,
    floor(randUniform(1, 100)) AS quantity,
    round(rand()%100 * 1.99, 2) AS price, 
    toDateTime('2024-08-23 00:00:00') + rand() % (number+1)-(((12 + randPoisson(12)) * 60) * 60)-(((12 + randPoisson(12))) * 60) as sale_date 
FROM numbers(1,200);
``` 
после вставки данных, можно увидеть, что MV содержит данные

![alt text](screens/image7.png)

### 4. Запросы к данным

запрос на извлечение данных из проекции (выполнялся в п.2)

```sql

SELECT
    product_id,
    sum(quantity) as total_quantity,
    round(sum(quantity*price),2) as total
FROM sales
GROUP BY product_id
ORDER BY product_id

```

запрос на извлечение данных из VM (выполнялся в п.3)

```sql
SELECT *
FROM sales_mv
```

### 5. Сравнение производительности

При сравнении производительности на наборе данных в 200 строк, выяснилось, что запросы п.4 быстрее выполняются из проекции, чем через MV (при первичном запуске)

Если повторять процедуру, то время постоянно разное.

время запроса из проекции  

![alt text](screens/image8.png)

время запроса из mv  

![alt text](screens/image9.png)


При добавлении данных (200 000) записей в таблицу sales, картина следующая:
Запрос на МV выполняется быстрее чем на проекции

время запроса из проекции

![alt text](screens/image11.png)

время запроса из mv

![alt text](screens/image10.png)



---

<u>**Дополнительные источники**</u>

1. <https://habr.com/ru/articles/657579/>
