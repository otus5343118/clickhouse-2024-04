INSERT INTO sales
SELECT
    number as id,
    floor(randUniform(1, 200)) AS product_id,
    floor(randUniform(1, 100)) AS quantity,
    round(rand()%100 * 1.99, 2) AS price, 
    toDateTime('2024-08-23 00:00:00') + rand() % (number+1)-(((12 + randPoisson(12)) * 60) * 60)-(((12 + randPoisson(12))) * 60) as sale_date 
FROM numbers(1,200);


