CREATE TABLE IF NOT EXISTS sales (
    `id`  UInt32,
    `product_id`  UInt32,
    `quantity`  UInt32,
    `price` Float32,
    `sale_date` DateTime,
) ENGINE = MergeTree() ORDER BY (`id`);



