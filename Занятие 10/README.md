# Домашнее задание

## Работа с джоинами и аггрегациями
### Цель:

Изучить синтаксис БД, отработать базовые команды взаимодействия, разобраться с простыми и сложными типами данных, джоинами

Тренируемые навыки:

- знание синтаксиса ClickHouse;  
- работа с джоинами, сложными типами данных, агрегацией, созданием витрин.


## Описание/Пошаговая инструкция выполнения домашнего задания:

<https://docs.google.com/document/d/1qIUv3RaRJ60boylxQF2rUdPcqNqAZa8V-yhGVbY6PBM/edit?usp=sharing>

---

### Запуск CH

поднять контейнер clickhouse

```bash
docker pull clickhouse/clickhouse-server

docker run -d -p 18123:8123 -p19000:9000 --name ch-server --ulimit nofile=262144:262144 clickhouse/clickhouse-server

```
запустить клиент

```bash
docker exec -it ch-server bash
clickhouse-client
```

### Создание БД и таблиц

```sql
CREATE DATABASE imdb;
CREATE TABLE imdb.actors
(
id UInt32,
first_name String,
last_name String,
gender FixedString(1)
) ENGINE = MergeTree ORDER BY (id, first_name, last_name,
gender);

CREATE TABLE imdb.genres
(
movie_id UInt32,
genre String
) ENGINE = MergeTree ORDER BY (movie_id, genre);

CREATE TABLE imdb.movies
(
id UInt32,
name String,
year UInt32,
rank Float32 DEFAULT 0
) ENGINE = MergeTree ORDER BY (id, name, year);

CREATE TABLE imdb.roles
(
actor_id UInt32,
movie_id UInt32,
role String,
created_at DateTime DEFAULT now()
) ENGINE = MergeTree ORDER BY (actor_id, movie_id);

```
![alt text](screens/image1.png)

### Вставка данных из S3

```sql
INSERT INTO imdb.actors
SELECT *
FROM
s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_actors.tsv.gz',
'TSVWithNames');

INSERT INTO imdb.genres
SELECT *
FROM
s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_movies_genres.tsv.gz',
'TSVWithNames');

INSERT INTO imdb.movies
SELECT *
FROM
s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_movies.tsv.gz',
'TSVWithNames');

INSERT INTO imdb.roles(actor_id, movie_id, role)
SELECT actor_id, movie_id, role
FROM
s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_roles.tsv.gz',
'TSVWithNames');

```

Посмотрим статистику таблиц после создания:

```sql
SELECT
    database,
    table,
    formatReadableSize(sum(data_compressed_bytes) AS size) AS compressed,
    formatReadableSize(sum(data_uncompressed_bytes) AS usize) AS uncompressed,
    round(usize / size, 2) AS compr_rate,
    sum(rows) AS rows,
    count() AS part_count
FROM system.parts
WHERE (active = 1) AND (database = 'imdb') AND (table LIKE '%')
GROUP BY
    database,
    table
ORDER BY size DESC;
```
![alt text](screens/image2.png)

### Построение запросов

#### a) найти жанры для каждого фильма

способ 1

```sql
SELECT
    movies.id,
    movies.name as movie,
    genre
FROM imdb.genres, imdb.movies
WHERE imdb.genres.movie_id = imdb.movies.id
ORDER BY movies.id
LIMIT 10;
```

![alt text](screens/image3.png)

способ 2

```sql
SELECT
    id, name, g.genre
FROM movies
LEFT JOIN imdb.genres g ON movies.id = g.movie_id
WHERE genre <> ''
ORDER BY id
LIMIT 10;
```
![alt text](screens/image5.png)



#### b) запросить все фильмы, у которых нет жанра

```sql
SELECT
    id, name, g.genre
FROM movies
LEFT JOIN imdb.genres g ON movies.id = g.movie_id
WHERE genre = ''
ORDER BY id
LIMIT 10;
```
![alt text](screens/image6.png)


#### с) объеденить каждую строку из таблицы "фильмы" с каждой строкой из таблицы "жанры"

```sql
SELECT
    id,
    name,
    g.movie_id,
    g.genre
FROM movies
LEFT JOIN imdb.genres g ON movies.id = g.movie_id
ORDER BY id
LIMIT 10;
```
![alt text](screens/image7.png)

#### d) Найти жанры для каждого фильма, НЕ используя INNER JOIN

```sql
SELECT
    movies.id,
    movies.name as movie,
    genre
FROM imdb.genres, imdb.movies
WHERE imdb.genres.movie_id = imdb.movies.id
ORDER BY movies.id
LIMIT 10;
```

![alt text](screens/image3.png)


#### e) Найти всех актеров и актрис, снявшихся в фильме в 2023 году

```sql
SELECT DISTINCT
    actor_id,
    first_name,
    last_name
FROM actors
LEFT JOIN imdb.roles r ON actors.id = r.actor_id
INNER JOIN
(
SELECT id, name, year
FROM movies
WHERE year = 2023
) as mov
USING id
ORDER BY actor_id

```

Фильмов, вышедших в 2023 году нет. Но если задать другой год, например, 2007, то предыдущий запрос покажет всех актеров и актрис:

фильмы 2023 г.
![alt text](screens/image8.png)

актрисы и актеры
![alt text](screens/image9.png)

#### f) Запросить все фильмы, у которых нет жанра, через ANTI JOIN

```sql
SELECT
    id, name, g.genre
FROM movies
ANTI LEFT JOIN imdb.genres g ON movies.id = g.movie_id
ORDER BY id
LIMIT 10;
```
![alt text](screens/image10.png)

---

### Дополнительные источники

1. <https://chistadata.com/knowledge-base/how-to-check-table-and-column-sizes-in-clickhouse/>
