# Домашнее задание

## Функции для работы с типами данных, агрегатный функции и UDF
### Цель:

Понять и применить агрегатные функции, функции, работающие с типами данных, и функции, определяемые пользователем (UDF) в ClickHouse.


### Набор данных:

Для выполнения этого домашнего задания вы будете использовать пример набора данных, представляющего транзакции электронной коммерции. 
Предположим, что у вас есть таблица `transactions` со следующей схемой:

```sql
CREATE TABLE transactions (
    transaction_id UInt32,
    user_id UInt32,
    product_id UInt32,
    quantity UInt8,
    price Float32,
    transaction_date Date
) ENGINE = MergeTree()
ORDER BY (transaction_id);
```

### Задание:

1. #### Агрегатные функции  
    1.1. Рассчитайте общий доход от всех операций.  
    1.2. Найдите средний доход с одной сделки.  
    1.3. Определите общее количество проданной продукции.  
    1.4. Подсчитайте количество уникальных пользователей, совершивших покупку.

2. #### Функции для работы с типами данных
    2.1. Преобразуйте `transaction_date` в строку формата   `YYYY-MM-DD`    
    2.2. Извлеките год и месяц из `transaction_date`.  
    2.3. Округлите `price` до ближайшего целого числа.  
    2.4. Преобразуйте `transaction_id` в строку.  

3. #### User-Defined Functions (UDFs)
    3.1. Создайте простую UDF для расчета общей стоимости транзакции.  
    3.2. Используйте созданную UDF для расчета общей цены для каждой транзакции.  
    3.3. Создайте UDF для классификации транзакций на «высокоценные» и «малоценные» на основе порогового значения (например, 100).  
    3.4. Примените UDF для категоризации каждой транзакции.  



## Описание/Пошаговая инструкция выполнения домашнего задания:

<https://docs.google.com/document/d/1hHqRv7lUUDmDkhzXrd0GIx7deF9Ez4mKpoYzHtDjoqQ/edit?usp=sharing>

---

### Запуск CH

поднять контейнер clickhouse

```bash
docker pull clickhouse/clickhouse-server

docker run -d -p 18123:8123 -p19000:9000 --name ch-server --ulimit nofile=262144:262144 clickhouse/clickhouse-server

```
запустить клиент

```bash
docker exec -it ch-server bash
clickhouse-client
```

### Создание таблицы

```sql
USE default;
CREATE TABLE transactions (
    transaction_id UInt32,
    user_id UInt32,
    product_id UInt32,
    quantity UInt8,
    price Float32,
    transaction_date Date
) ENGINE = MergeTree()
ORDER BY (transaction_id);
```

### Вставка данных 

Сгенерируем случайные данные

```sql
INSERT INTO transactions (transaction_id, user_id, product_id, quantity, price, transaction_date) 
SELECT
    Floor(randUniform(1,6)),
    Floor(randUniform(1,10)),
    Floor(randUniform(1,6)),
    Floor(randUniform(1,101)),
    round(randUniform(1,101),2),
    now() 
FROM system.numbers
LIMIT 20;
```

![alt text](screens/image1.png)

### Агрегатные функции

#### 1.1 Расчитайте общий доход от всех операций

```sql
SELECT
    sum(price*quantity) as total_income
FROM transactions;
```
![alt text](screens/image2.png)

#### 1.2 Найдите средний доход с одной сделки

за одну сделку будем предполагать все операции, которые относятся к одной транзакции

```sql
SELECT
    transaction_id,                      -- транзакция
    sum(price*quantity) as total_income, -- cумма сделки
    count(transaction_id) as cnt_op,     -- количество операций в транзакции
    avg(price*quantity) as avg_total     -- среднее
FROM transactions
GROUP BY transaction_id;
```
![alt text](screens/image3.png)

#### 1.3 Определите общее количество проданной продукции.

```sql
SELECT
    sum(quantity) as total
FROM transactions;
```
![alt text](screens/image4.png)

#### 1.4 Подсчитайте количество уникальных пользователей, совершивших покупку.

```sql
SELECT uniq(user_id)
FROM transactions;
```
![alt text](screens/image5.png)

### Функции для работы с типами данных

Одним запросом выполним этот пункт

```sql
SELECT
    toString(transaction_date) as str_date,
    toYear(transaction_date) as year,
    toMonth(transaction_date) as month,
    round(price,0) as price,
    toString(transaction_id) as id
FROM transactions
WHERE transaction_id=5;
```
![alt text](screens/image5.png)

### User-Defined Functions (UDFs)

создадим UDF функцию, которая подсчитывает общую стоимоисть транзакции
```sql
CREATE FUNCTION total_sum_trans AS (price, quantity) -> price*quantity;
```
подсчитаем цену каждой транзакции

```sql
SELECT
    transaction_id, 
    sum(total_sum_trans(price,quantity))
FROM transactions
GROUP BY transaction_id;

```
![alt text](screens/image6.png)

создадим UDF для классификации транзакции

```sql
CREATE FUNCTION class_trans AS (summa, value) -> if(summa > value,'high', 'low');
```

проверим, к какому классу относится транзакция, уровень проверки 1000
```sql
SELECT
    transaction_id,
    sum(total_sum_trans(price,quantity)),
    class_trans(sum(total_sum_trans(price,quantity)),1000)
FROM transactions
GROUP BY transaction_id;
```

![alt text](screens/image7.png)

---

### Дополнительные источники

1. <https://chistadata.com/knowledge-base/how-to-check-table-and-column-sizes-in-clickhouse/>
